FROM python:3.9-slim-bullseye
LABEL maintainer="William Arias"
COPY .    /app/
WORKDIR /app
RUN pip3 install -r requirements.txt
EXPOSE 8501
ENTRYPOINT ["python3"]
CMD ["test-gpu.py"]
